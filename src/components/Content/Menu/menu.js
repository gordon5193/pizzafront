import React, { Component } from "react";
import { CardGroup } from 'semantic-ui-react'
import pizza1 from './img/margaritta.jpg'
import pizza2 from './img/pestuno.jpg'
import pizza3 from './img/quadro.jpg'
import pizza4 from './img/capricciosa.jpg'
import pizza5 from './img/salami.jpg'
import pizza6 from './img/chicken.jpg'
import pizza7 from './img/bbq.jpg'
import pizza8 from './img/meatball.jpg'
import './menu.css';
import MenuItem from '../Menu-item'

const pizzas = [
    {
        key: 'marg',
        img: pizza1,
        size: 36,
        ing: 'Tomatoes, Extra- Virgin Olive Oil, Pepper',
    },
    {
        key: 'pest',
        img: pizza2,
        size: 34,
        ing: 'Pesto Base, Mozarella, Gouda, Spinach, Pepper',
    },
    {
        key: 'quad',
        img: pizza3,
        size: 36,
        ing: 'Tomatoes, Mozzarella, Gorgonzola, Provolone, Parmesan, Basil',
    },
    {
        key: 'capr',
        img: pizza4,
        size: 34,
        ing: 'Tomatoes, Mozarella, Italian ham, Mushrooms, Artichokes, Olives',
    },
    {
        key: 'sal',
        img: pizza5,
        size: 32,
        ing: 'Tomatoes, Mozarella, Origano, Salami, Ham, Pepper',
    },
    {
        key: 'chic',
        img: pizza6,
        size: 32,
        ing: 'Tomatoes, Mozarella, Chicken, Onions, Paprika, Artichokes',
    },
    {
        key: 'bbq',
        img: pizza7,
        size: 30,
        ing: 'Tomatoes, Mozarella, Olives, Paprika, Salami, BBQ Sauce',
    },
    {
        key: 'meat',
        img: pizza8,
        size: 30,
        ing: 'Tomatoes, Mozarella, Salami, Meatballs (Beef), Pepper',
    }
]

export default class Menu extends Component {
    render() {
        return (
            <CardGroup centered stackable itemsPerRow={4}>
                {pizzas.map((item, i) => <MenuItem key={i} Details={item} Quantity={this.props.orderInfo[item.key][0]} Price={this.props.orderInfo[item.key][1]} Name={this.props.orderInfo[item.key][2]} manageOrder={this.props.manageOrder} />)}
            </CardGroup>
        )
    }
}