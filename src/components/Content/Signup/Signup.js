import React, { Component } from "react";
import { Segment, Input, Button, Form, Header, Icon } from 'semantic-ui-react'

export default class Signup extends Component {
    render() {

        return (
            <span>
                <Button onClick={this.props.Back2Login}><Icon name="angle double left" />Back to Login</Button>
                <Segment textAlign="left">
                    <Form>
                        <Form.Group widths='equal'>
                            <Form.Field required
                                id='username'
                                control={Input}
                                label='Login'
                                placeholder='Myusername'
                                onChange={this.props.inputChange}
                                type="text"
                            />
                            <Form.Field required
                                id='pass'
                                control={Input}
                                label='Password'
                                placeholder='superpass'
                                onChange={this.props.inputChange}
                                type="password"
                            />
                            <Form.Field required
                                id='repeatpass'
                                control={Input}
                                label='Repeat Password'
                                placeholder='superpass'
                                onChange={this.props.inputChange}
                                type="password"
                            />
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Field required
                                id='first'
                                control={Input}
                                label='First name'
                                placeholder='Gordon'
                                onChange={this.props.inputChange}
                                type="text"
                            />
                            <Form.Field required
                                id='last'
                                control={Input}
                                label='Last name'
                                placeholder='Freeman'
                                onChange={this.props.inputChange}
                                type="text"

                            />
                            <Form.Field required
                                id='email'
                                control={Input}
                                label='Email'
                                placeholder='johndoe@mail.com'
                                onChange={this.props.inputChange}
                                type="email"
                            />
                            <Form.Field required
                                id='phone'
                                control={Input}
                                label='Phone'
                                width={12}
                                placeholder='+79991234567'
                                onChange={this.props.inputChange}
                                type="phone"
                            />
                        </Form.Group>
                        {/* <Header>Default Adress</Header>
                        <Form.Group widths='equal'>
                            <Form.Field
                                id='street'
                                control={Input}
                                label='Street'
                                placeholder='Nova Prospect'
                                onChange={this.props.inputChange}
                                type="text"
                            />
                            <Form.Field
                                id='house'
                                control={Input}
                                label='House'
                                width={3}
                                placeholder='123'
                                min="1" max="100"
                                onChange={this.props.inputChange}
                                type="number"
                            />
                            <Form.Field
                                id='build'
                                control={Input}
                                label='Buiding'
                                placeholder='3'
                                width={3}
                                min="1" max="100"
                                onChange={this.props.inputChange}
                                type="number"
                            />
                            <Form.Field
                                id='apt'
                                control={Input}
                                label='Flat'
                                placeholder='123'
                                width={3}
                                onChange={this.props.inputChange}
                                type="number"
                            />
                        </Form.Group> */}
                    </Form >
                </Segment>
                <Button type="submit" onClick={this.props.onRegister}>Sign Up</Button>
            </span>
        )
    }
}