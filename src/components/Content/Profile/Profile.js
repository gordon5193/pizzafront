import React, { Component } from "react";
import { Segment, Button, Header, Message } from 'semantic-ui-react'
import Displayorder from "../DisplayOrder"

export default class Profile extends Component {

    state = {
        theorders: null
    }

    chunk = (arr, size) =>
        Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
            arr.slice(i * size, i * size + size)
        );

    parseContent = content => {
        return this.chunk(content.filter(function (elem, index) {
            return index % 4 !== 0
        }), 3);
    }

    parseShipment = ship => {
        const theship = ship.map(p => p.charAt(0).toUpperCase() + p.slice(1));
        return this.chunk(theship, 2);
    }

    loadOrders = (user) => {
        fetch('http://localhost:5000/getorders', {
            method: 'POST',
            body: JSON.stringify({ user: user }),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            if (data.orders.length == 0) {
                this.setState(({ theorders }) => {
                    const theorder = "nothing"
                    return {
                        theorders: theorder
                    };
                });
            }
            else {
                const thisword = this;
                let allorders = []
                data.orders.map(function (d) {
                    const id = d.id;
                    const content = thisword.parseContent(d.content.split(','));
                    const ship = thisword.parseShipment(d.ship.split(','));
                    const thedate = new Date(d.times * 1000);
                    allorders.push({
                        id: id,
                        ship: ship,
                        content: content,
                        thedate: thedate
                    })
                })
                this.setState(({ theorders }) => {
                    return {
                        theorders: allorders
                    };
                });
            }
        });
    }

    orderLoadController = () => {
        if (this.state.theorders === null) {
            return (
                <span>
                    {this.loadOrders(this.props.user)}
                </span>
            )
        }
        else if (this.state.theorders === "nothing") {
            return (
                <span>
                    <Message> You have no orders in this account </Message >
                </span>
            )
        }
        else {
            const orderToView = this.state.theorders.map((d) =>
                <Segment basic>
                    <Displayorder order={d} />

                </Segment>
            );
            return (
                <span>
                    {orderToView}
                </span>
            )

        }
    }


    render() {
        return (
            <Segment textAlign="left">
                <Button onClick={this.props.logout}>Logout</Button>
                <Header>Your Orders:</Header>
                {this.orderLoadController()}
            </Segment>
        )
    }
}