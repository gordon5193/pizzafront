import React, { Component } from "react";
import { Grid, Button, Form, Message, Segment } from 'semantic-ui-react'
import Signup from "../Signup";
import Profile from "../Profile";

export default class Login extends Component {
    state = {
        current: 'login',
        register: {
            username: null,
            pass: null,
            repeatpass: null,
            first: null,
            last: null,
            email: null,
            phone: null,
            street: null,
            house: null,
            build: null,
            apt: null,
        },
        login: {
            username: null,
            pass: null
        },
        message: null
    }

    FillLogin = (e) => {
        const target = e.target.getAttribute('id');
        const content = e.target.value
        this.setState(({ login }) => {
            let returnval = login;
            returnval[target] = content
            return {
                login: returnval
            }
        });
    }

    FillInfo = (e) => {
        const target = e.target.getAttribute('id');
        const content = e.target.value
        this.setState(({ register }) => {
            let returnval = register;
            returnval[target] = content
            return {
                register: returnval
            }
        });
    }

    checkRepeat = (data) => {
        if (data.found) {
            const exist = data.found[0]
            if (exist.email === this.state.register.email) {
                alert('This email is already registered')
            }
            else if (exist.username === this.state.register.username) {
                alert('This username name already exist')
            }
            else {
                alert('This phone is registered to other user')
            }
        }
        else {
            alert('Registered successfully')
            this.switchForm();
        }

    }

    userRegister = e => {
        e.preventDefault()
        const regFileds = Object.entries(this.state.register);
        for (let index = 0; index < 7; index++) {
            if (regFileds[1][index] === null) {
                console.log('not filled', index)
                break;
            }
        }
        fetch('http://localhost:5000/register', {
            method: 'POST',
            body: JSON.stringify(this.state.register),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => this.checkRepeat(data));
    }

    userLogin = e => {
        e.preventDefault()
        fetch('http://localhost:5000/login', {
            method: 'POST',
            body: JSON.stringify(this.state.login),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            if (data.status === 'wrong') {
                alert('bad pass')
            }
            else {
                this.props.loginToAccount(this.state.login.username)
            }
        });
    }

    switchForm = (e) => {
        this.setState(({ current }) => {
            if (current === 'login') {
                return {
                    current: 'signup'
                };
            }
            else {
                return {
                    current: 'login'
                };
            }
        });
    }

    formController = (user) => {
        if (user !== null) {
            return (
                <Profile logout={this.props.logout} user={user} />
            )
        }
        else {
            if (this.state.current === 'login') {
                return (
                    <Grid centered >
                        <Segment basic>
                            <Form onSubmit={this.userLogin}>
                                <Form.Field>
                                    <label>Login</label>
                                    <input placeholder='Username' id="username" onChange={this.FillLogin} />
                                </Form.Field>
                                <Form.Field>
                                    <label>Password</label>
                                    <input placeholder='Password' id="pass" type="password" onChange={this.FillLogin} />
                                </Form.Field>
                                <Button onClick={this.userLogin} type='submit'>Submit</Button>
                            </Form>
                            <Message>
                                <Message.Header>Don't have an account yet?</Message.Header>
                                <p><a onClick={this.switchForm} href='#'>Sign up</a> here</p>
                            </Message>
                        </Segment>
                    </Grid>
                )
            }
            else if (this.state.current === 'signup') {
                return (
                    <Signup Back2Login={this.switchForm} inputChange={this.FillInfo} onRegister={this.userRegister} />
                )
            }
        }

    }

    render() {
        return (
            <span>
                {this.formController(this.props.user)}
            </span>
        )
    }
}