import React, { Component } from "react";
import { Form, Input, TextArea, Button, Select } from 'semantic-ui-react'

export default class ContactForm extends Component {


    requestCustomerInfo = (user) => {
        if (user !== null) {
            return (
                <span></span>
            )
        }
        else {
            return (


                <Form.Group widths='equal'>
                    <Form.Field required
                        id='first'
                        control={Input}
                        label='First name'
                        placeholder='Gordon'
                        onChange={this.props.inputChange}
                        type="text"
                    />
                    <Form.Field required
                        id='last'
                        control={Input}
                        label='Last name'
                        placeholder='Freeman'
                        onChange={this.props.inputChange}
                        type="text"
                    />
                    <Form.Field required
                        id='email'
                        control={Input}
                        label='Email'
                        placeholder='johndoe@mail.com'
                        onChange={this.props.inputChange}
                        type="email"
                    />
                    <Form.Field required
                        id='phone'
                        control={Input}
                        label='Phone'
                        width={12}
                        placeholder='+79991234567'
                        onChange={this.props.inputChange}
                        type="phone"
                    />
                </Form.Group>
            )
        }
    }

    render() {
        return (
            <Form id='customerInfo'>
                {this.requestCustomerInfo(this.props.user)}
                <Form.Group widths='equal'>
                    <Form.Field required
                        id='street'
                        control={Input}
                        label='Street'
                        placeholder='Nova Prospect'
                        onChange={this.props.inputChange}
                        type="text"
                    />
                    <Form.Field required
                        id='house'
                        control={Input}
                        label='House'
                        width={3}
                        placeholder='123'
                        onChange={this.props.inputChange}
                        type="number"
                    />
                    <Form.Field
                        id='build'
                        control={Input}
                        label='Buiding'
                        placeholder='3'
                        width={3}
                        onChange={this.props.inputChange}
                        type="number"
                    />
                    <Form.Field
                        id='apt'
                        control={Input}
                        label='Flat'
                        placeholder='123'
                        width={3}
                        onChange={this.props.inputChange}
                        type="number"
                    />
                </Form.Group>
                <Form.Field
                    id='comment'
                    control={TextArea}
                    label='Comments'
                    placeholder='Comments to order (Where to left order, no cheese in pizza, etc)'
                    onChange={this.props.inputChange}
                    type="text"
                />
            </Form >
        )
    }
}