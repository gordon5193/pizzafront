import React, { Component } from "react";
import { Header, Grid, Message, Item, Button, Segment, Divider } from 'semantic-ui-react'
import ContacForm from "./ContactForm"
import PopupComplete from "./PopupComplete"

export default class Cart extends Component {

    state = {
        customer: {
            first: null,
            last: null,
            email: null,
            phone: null,
            street: null,
            house: null,
            build: null,
            apt: null,
            comment: null
        }
    }
    FillInfo = (e) => {
        const target = e.target.getAttribute('id');
        const content = e.target.value
        console.log(target, content)
        this.setState(({ customer }) => {
            let returnval = customer;
            returnval[target] = content
            return {
                customer: returnval
            }
        });
    }

    CartCleanup = (e) => {
        e.preventDefault();
        this.setState(({ customer }) => {
            const returnval = {
                first: null,
                last: null,
                email: null,
                phone: null,
                street: null,
                house: null,
                build: null,
                apt: null,
                comment: null
            }
            return {
                customer: returnval
            }
        });
        this.props.cleanUpCart()
    }

    OrderPlacement = (e) => {
        e.preventDefault();
        const user = this.props.user
        const shipInfo = Object.entries(this.state.customer).filter(p => p[1] !== null)
        const content = Object.entries(this.props.orderInfo).filter(p => p[1][0] !== 0);
        const timeplace = Math.floor(Date.now() / 1000)
        fetch('http://localhost:5000/createorder', {
            method: 'POST',
            body: JSON.stringify(
                { user: user, ship: shipInfo, content: content, time: timeplace }),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            alert(`Orders id: ${data.status}`)
        });
    }

    manage = (e) => {
        e.preventDefault();
        const action = e.target.textContent;
        const key = e.target.getAttribute("data-id");
        this.props.manageOrder(key, action);
    }

    SubTotal = order => {
        const total = Object.entries(order).filter(p => p[1][0] !== 0);
        if (total.length > 0) {
            return Object.values(order).filter(p => p[0] > 0)
                .map(item => item[0] * item[1])
                .reduce((a, b) => a + b);
        }
        else {
            return 0;
        }
    }

    CartController = order => {
        const inCart = Object.entries(order).filter(p => p[1][0] !== 0);
        if (inCart.length == 0) {
            return (
                <Message>Your cart is empty at the moment</Message>
            )
        }
        else {
            const theclass = this;
            const toOrder = inCart.map(function (d) {
                return (
                    <Grid.Row>
                        <Grid.Column textAlign="left" verticalAlign='middle'>
                            <Header size='small' key={d[1][2]}>{d[1][2]}</Header>
                        </Grid.Column>
                        <Grid.Column textAlign="left" verticalAlign='middle'>
                            <Header size='small' key={d[1][1]}>{d[1][1]}€ ({d[1][1] * d[1][0] * 1.2} $)</Header>
                        </Grid.Column>
                        <Grid.Column textAlign="left" verticalAlign='middle'>
                            <Button.Group size="mini">
                                <Button size="mini" data-id={d[0]} onClick={theclass.manage}>-</Button>
                                <Button.Or size="mini" text={d[1][0]} />
                                <Button size="mini" data-id={d[0]} onClick={theclass.manage}>+</Button>
                            </Button.Group>
                        </Grid.Column>
                        <Grid.Column textAlign="left" verticalAlign='middle'>
                            <Header size='small'>{d[1][1] * d[1][0]} € ({d[1][1] * d[1][0] * 1.2} $)</Header>
                        </Grid.Column>
                    </Grid.Row >
                )
            });
            return (
                <span>
                    <Header>Your Order</Header>
                    <Segment textAlign="left">
                        <Grid columns={4}>
                            <Grid.Row>
                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                    <Header>Pizza</Header>
                                </Grid.Column>
                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                    <Header>Price</Header>
                                </Grid.Column>
                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                    <Header>Quantity</Header>
                                </Grid.Column>
                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                    <Header>Total Price</Header>
                                </Grid.Column>
                            </Grid.Row>
                            {toOrder}
                        </Grid>
                    </Segment>
                    <Header>Customer Information</Header>
                    <Segment textAlign="left">
                        <ContacForm inputChange={this.FillInfo} user={this.props.user} />
                    </Segment>
                    <Header>Total</Header>
                    <Segment textAlign="left">
                        <Header size='small'>Subtotal: {this.SubTotal(this.props.orderInfo)}€ ({this.SubTotal(this.props.orderInfo) * 1.2} $)</Header>
                        <Header size='small'>Contactless Delivery: {3}€ (3.5)$</Header>
                        <Header size='small'>Total: {this.SubTotal(this.props.orderInfo) + 3}€ ({(this.SubTotal(this.props.orderInfo) * 1.2) + 3.5} $)</Header>
                        <Button onClick={this.CartCleanup}>Cancel</Button>
                        <Button onClick={this.OrderPlacement}>Order</Button>
                    </Segment>
                    <div></div>
                </span>
            );
        }
    }
    render() {
        return (
            <span>
                {this.CartController(this.props.orderInfo)}
            </span >


        )
    }
}