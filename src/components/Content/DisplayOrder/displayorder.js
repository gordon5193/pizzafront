import React, { Component } from "react";
import { Button, Segment, Form, Grid, Message, Header, Divider } from 'semantic-ui-react'

export default class Displayorder extends Component {
    render() {
        return (
            <span>
                <div>
                    <Header>Order #{this.props.order.id}</Header>
                </div>
                <Grid columns={2}>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <Header>Shipping Information</Header>
                                <Grid columns={2}>
                                    <Grid.Row>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'>Order Time:</Header>
                                        </Grid.Column>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'>
                                                {this.props.order.thedate.toGMTString()}
                                            </Header>
                                        </Grid.Column>
                                    </Grid.Row >
                                    {this.props.order.ship.map(function (elem) {
                                        return (
                                            <Grid.Row>
                                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                                    <Header size='small'>{elem[0]}</Header>
                                                </Grid.Column>
                                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                                    <Header size='small'>{elem[1]}</Header>
                                                </Grid.Column>
                                            </Grid.Row >
                                        )
                                    })}
                                </Grid>
                            </Segment>
                        </Grid.Column>
                        <Grid.Column>
                            <Segment>
                                <Header>Order Content</Header>
                                <Grid columns={3}>
                                    <Grid.Row>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'>Pizzas</Header>
                                        </Grid.Column>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'>Price</Header>
                                        </Grid.Column>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'>Total</Header>
                                        </Grid.Column>
                                    </Grid.Row >
                                    {this.props.order.content.map(function (elem) {
                                        return (
                                            <Grid.Row>
                                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                                    <Header size='small'>{elem[0]} x {elem[2]}</Header>
                                                </Grid.Column>
                                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                                    <Header size='small'>{elem[1]}€ ({elem[1] * 1.2})$</Header>
                                                </Grid.Column>
                                                <Grid.Column textAlign="left" verticalAlign='middle'>
                                                    <Header size='small'>{elem[1] * elem[0]}€ ({elem[1] * elem[0] * 1.2})$</Header>
                                                </Grid.Column>
                                            </Grid.Row >
                                        )
                                    })}
                                    <Divider></Divider>
                                    <Grid.Row>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'>Total (incl. Delivery)</Header>
                                        </Grid.Column>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'></Header>
                                        </Grid.Column>
                                        <Grid.Column textAlign="left" verticalAlign='middle'>
                                            <Header size='small'>
                                                {this.props.order.content.map(item => item[0] * item[1]).reduce((a, b) => a + b)} €
                                        ({this.props.order.content.map(item => item[0] * item[1]).reduce((a, b) => a + b) * 1.2}) $
                                    </Header>
                                        </Grid.Column>
                                    </Grid.Row >
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </span>
        )
    }
}