import React, { Component } from "react";
import { Container } from 'semantic-ui-react'
import Main from "./Main"
import Menu from "./Menu"
import Login from "./Login"
import Delivery from "./Delivery"
import Cart from "./Cart"

export default class Content extends Component {
    PageController = props => {
        switch (props.currentPage) {
            case 'home':
                return <Main />
            case 'menu':
                return <Menu orderInfo={props.orderStatus} manageOrder={props.manageOrder} />
            case 'login':
                return <Login
                    loginToAccount={props.loginToAccount}
                    logout={props.logout}
                    user={props.user} />
            case 'ship':
                return <Delivery />
            case 'cart':
                return <Cart user={props.user} orderInfo={props.orderStatus} manageOrder={props.manageOrder} cleanUpCart={props.cleanUpCart} />
            default:
                return <Main />
        }
    }

    render() {
        return (
            <Container>
                {this.PageController(this.props)}
            </Container>
        )
    }
}