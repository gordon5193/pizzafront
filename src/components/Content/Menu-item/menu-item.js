import React, { Component } from "react";
import { Card, Icon, Image, Item, Divider, Button } from 'semantic-ui-react'
import './menu-item.css';
import { act } from "react-dom/test-utils";


export default class MenuItem extends Component {

    manage = (e) => {
        e.preventDefault();
        const action = e.target.textContent;
        this.props.manageOrder(this.props.Details.key, action);
    }

    ItemController = quant => {
        if (quant) {
            return (
                <Button.Group>
                    <Button disabled className="in-cart"><Icon name='cart' />In-Cart</Button>
                    <Button onClick={this.manage} className="qty-button">-</Button>
                    <Button.Or text={quant} />
                    <Button onClick={this.manage} className="qty-button">+</Button>
                </Button.Group>
            )
        }
        else {
            return (
                <Button className="cartlink" onClick={this.manage}>
                    <a><Icon name='cart' />Add to Cart</a>
                </Button>
            )
        }
    }

    render() {
        return (
            <Card>
                <Image src={this.props.Details.img} wrapped ui={false} />
                <Card.Content>
                    <Card.Header>{this.props.Name}</Card.Header>
                    <Card.Meta>
                        <span>Ø {this.props.Details.size} cm</span>
                    </Card.Meta>
                    <Card.Description>
                        {this.props.Details.ing}
                    </Card.Description>
                </Card.Content>
                <Card.Content extra textAlign="center">
                    <Item>
                        <Card.Header as='h3'>
                            {this.props.Price} € ( {Math.round(this.props.Price * 1.2)} $)
                        </Card.Header>
                    </Item>
                    <Divider></Divider>
                    {this.ItemController(this.props.Quantity)}
                </Card.Content>
            </Card>
        )
    }
}