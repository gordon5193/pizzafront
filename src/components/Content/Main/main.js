import React, { Component } from "react";
import { Grid, Segment, List, Header, Image } from 'semantic-ui-react'
import logo from './img/delivery.jpg' // relative path to image 
import theMap from "../Map"

const mapsurl = "https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d21735.347767335206!2d15.433728000000002!3d47.0810624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sat!4v1597037531030!5m2!1sru!2sat"

export default class Main extends Component {
    render() {
        return (
            <Grid columns={2} divided stackable>
                <Grid.Row >
                    <Grid.Column>
                        <Image
                            src={logo}
                            as='a'
                            href='http://google.com'
                            target='_blank'
                            fluid
                            bordered
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Segment textAlign="left">
                            <Header>Our Advantages:</Header>
                            <List size="large">
                                <List.Item>
                                    <List.Icon name='time' />
                                    <List.Content>
                                        <List.Header>Open the whole week from 08:00 to 22:00</List.Header>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='euro sign' />
                                    <List.Content>
                                        <List.Header>Providing best value to our customers</List.Header>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='pagelines' />
                                    <List.Content>
                                        <List.Header>Using eco-friendly packages</List.Header>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='hand point right' />
                                    <List.Content>
                                        <List.Header>Guaranting order delivery in 60 minutes</List.Header>
                                    </List.Content>
                                </List.Item>
                            </List>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid >
        )
    }
}