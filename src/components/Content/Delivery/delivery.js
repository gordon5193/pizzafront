import React, { Component } from "react";
import { Button, Segment, Form, Grid, Message, Header, Divider } from 'semantic-ui-react'
import Displayorder from "../DisplayOrder"

export default class Delivery extends Component {

    state = {
        current: 'form',
        order: null
    }

    handleChange = e => {
        const content = e.target.value;
        this.setState(({ order }) => {
            return {
                order: content
            };
        });
    };

    cleanUpInfo = () => {
        this.setState(({ order }) => {
            return {
                order: null
            }
        })
    }
    switchForm = () => {
        this.setState(({ current }) => {
            if (current === 'form') {
                return {
                    current: 'order'
                };
            }
            else {
                this.cleanUpInfo();
                return {
                    current: 'form'
                };
            }
        });

    }

    chunk = (arr, size) =>
        Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
            arr.slice(i * size, i * size + size)
        );

    parseContent = content => {
        return this.chunk(content.filter(function (elem, index) {
            return index % 4 !== 0
        }), 3);
    }

    parseShipment = ship => {
        const theship = ship.map(p => p.charAt(0).toUpperCase() + p.slice(1));
        return this.chunk(theship, 2);
    }

    loadOrder = e => {
        e.preventDefault()
        fetch('http://localhost:5000/checkorder', {
            method: 'POST',
            body: JSON.stringify({ order: this.state.order }),
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => res.json()).then(data => {
            if (data.orders.length == 0) {
                alert('Nothing found for the giver order number');
            }
            else {
                const id = this.state.order;
                const content = this.parseContent(data.orders[0].content.split(','));
                const ship = this.parseShipment(data.orders[0].ship.split(','));
                const thedate = new Date(data.orders[0].times * 1000);
                this.setState(({ order }) => {
                    const theorder = {
                        id: id,
                        content: content,
                        ship: ship,
                        thedate: thedate
                    }
                    return {
                        order: theorder
                    };
                });
                console.log(this.state.order.ship)
                this.switchForm();
            }
        });
    }

    formController = () => {
        if (this.state.current === 'form') {
            return (
                <span>
                    <Form onSubmit={this.loadOrder}>
                        <Form.Field>
                            <label>Enter the Order Number</label>
                            <input onChange={this.handleChange} />
                        </Form.Field>
                        <Button type='submit'>Check</Button>
                    </Form>
                </span>
            )
        }
        else {
            return (
                <span>
                    <Button onClick={this.switchForm}>Check another order</Button>
                    <Displayorder order={this.state.order} />
                </span>

            )
        }
    }


    render() {
        return (
            <Grid centered >
                <Segment basic>
                    <span>
                        {this.formController()}
                    </span>
                </Segment>
            </Grid >
        )
    }
}