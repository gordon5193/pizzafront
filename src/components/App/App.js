import React, { Component } from "react";
import './App.css';
import { Container } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css'
import Head from "../Header"
import MainMenu from "../MainMenu"
import Content from "../Content"
import Footer from "../Footer"

export default class App extends Component {
  state = {
    activeItem: 'home',
    order: {
      marg: [0, 7, 'Margaritta'],
      pest: [0, 10, 'Pestuno'],
      quad: [0, 8, 'Quadro'],
      capr: [0, 9, 'Capricciosa'],
      sal: [0, 11, 'Salami'],
      chic: [0, 9, 'Chicky'],
      bbq: [0, 11, 'BBQer'],
      meat: [1, 10, 'Meatball Pizza']
    },
    user: null
  }

  onPageSet = page => {
    this.setState(({ activeItem }) => {
      return {
        activeItem: page
      };
    });
  };

  onAddToCart = (key, action) => {
    this.setState(({ order }) => {
      let returnval = order;
      if (action == '-') {
        returnval[key][0]--
      }
      else {
        returnval[key][0]++
      }
      return {
        order: returnval
      };
    });
  }

  cleanUpCart = () => {
    this.setState(({ order }) => {
      const reset = {
        marg: [0, 7, 'Margaritta'],
        pest: [0, 10, 'Pestuno'],
        quad: [0, 8, 'Quadro'],
        capr: [0, 9, 'Capricciosa'],
        sal: [0, 11, 'Salami'],
        chic: [0, 9, 'Chicky'],
        bbq: [0, 11, 'BBQer'],
        meat: [0, 10, 'Meatball Pizza']
      }
      return {
        order: reset
      }
    });
  }

  loginToAccount = (username) => {
    this.setState(({ user }) => {
      return {
        user: username
      }
    });
  }

  logout = () => {
    this.setState(({ user }) => {
      return {
        user: null
      }
    });
  }

  render() {
    const { activeItem, order, user } = this.state;
    const itemsCount = Object.values(order).map(item => item[0]).reduce((a, b) => a + b);

    return (
      <div className="App">
        <Container textAlign='center'>
          <Head />
          <MainMenu pageSwitch={this.onPageSet} counter={itemsCount} user={user} />
          <Content currentPage={activeItem}
            orderStatus={order}
            manageOrder={this.onAddToCart}
            cleanUpCart={this.cleanUpCart}
            loginToAccount={this.loginToAccount}
            logout={this.logout}
            user={user} />
          <Footer />
        </Container>
      </div>
    );
  }
}
