import React, { Component } from "react";
import { Menu, Icon, Label, Message } from 'semantic-ui-react'
import './mainmenu.css';

export default class MainMenu extends Component {

    state = { activeItem: 'home' }

    clickHandle = (e, { name }) => {
        this.setState({ activeItem: name })
        this.props.pageSwitch(name);
    }

    usernameSet = username => {
        if (username === null) {
            return (
                <span>Login | Register</span>
            )
        }
        else {
            return (
                <span>Welcome, {username}</span>
            )
        }
    }

    render() {
        const { activeItem } = this.state
        return (
            <Menu stackable secondary widths={5}>
                <Menu.Item as='a' className="menu-item"
                    name='home'
                    active={activeItem === 'home'}
                    onClick={this.clickHandle}
                >
                    <Icon name='home' />
                                Home
                            </Menu.Item>

                <Menu.Item as='a' className="menu-item"
                    name='menu'
                    active={activeItem === 'menu'}
                    onClick={this.clickHandle}
                >
                    <Icon name='utensils' />
                                Menu
                                </Menu.Item>
                <Menu.Item as='a' className="menu-item"
                    name='ship'
                    active={activeItem === 'ship'}
                    onClick={this.clickHandle}
                >
                    <Icon name='shipping fast' />
                                Order check
                                </Menu.Item>
                <Menu.Item as='a' className="menu-item"
                    name='login'
                    active={activeItem === 'login'}
                    onClick={this.clickHandle}
                >
                    <Icon name='user' />
                    <span>{this.usernameSet(this.props.user)}</span>
                </Menu.Item>
                <Menu.Item as='a' className="menu-item"
                    name='cart'
                    active={activeItem === 'cart'}
                    onClick={this.clickHandle}>
                    <Icon name='cart' />Cart<Label color='grey'>{this.props.counter}</Label>
                </Menu.Item>
            </Menu >
        )
    }
}