import React, { Component } from "react";
import { Header, Icon } from 'semantic-ui-react'

export default class Head extends Component {
    render() {
        return (
            <Header as='h2' icon textAlign='center'>
                <Icon name='gripfire' color="orange" />
                <Header.Content>Fire Pizza
                    <Header.Subheader padded="true">HOT. FRESH. YOURS.</Header.Subheader>
                </Header.Content>
            </Header>
        )
    }
}